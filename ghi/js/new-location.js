window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/states";

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        const stateSelector = document.getElementById('state');

        for (let state of data.states) {
            const optionTag = document.createElement('option');
            optionTag.innerHTML = state.name;
            optionTag.value = state.abbreviation;
            stateSelector.appendChild(optionTag);
        };
    };

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-type': 'application.json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
        }
        
    });
});
