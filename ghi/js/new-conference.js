window.addEventListener("DOMContentLoaded", async () => {

    const url = "http://localhost:8000/api/locations/";

    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const locations = data.locations;
        const locationSelector = document.getElementById('location');

        for (let location of locations) {
            const optionTag = document.createElement('option');
            optionTag.innerHTML = location.name;
            optionTag.value = location.id;
            locationSelector.appendChild(optionTag);
        };
    };


    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener("submit", async event =>{
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                "Content-type": "application.json",
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
        };


    });
});
