function createCard(name, description, pictureUrl, dates, location) {
    return `
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-secondary">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">${dates}</div>
      </div>
    `;
  }

function createAlert(error) {
    return `
    <div class="alert alert-dark" role="alert">${error}</div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if(!response.ok) {
            const error = new Error('Resoponse not ok');
            const main = document.querySelector('.container');
            const html = createAlert(error);
            main.innerHTML += html

        } else {
            const data = await response.json();
            let count = 1;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pistureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts).toLocaleDateString();
                    const end = new Date(details.conference.ends).toLocaleDateString();
                    const dates = `${start} - ${end}`
                    const location = details.conference.location.name
                    const html = createCard(name, description, pistureUrl, dates, location);
                    let column = document.querySelector(`.col${count}`);

                    column.innerHTML += html;
                    count++;
                    if( count === 4) {
                        count = 1;
                    }

                }
            }

        }
    } catch (e) {
        const main = document.querySelector(`.container`);
        const error = e;
        const html = createAlert(error);
        main.innerHTML += html;

    }

});
