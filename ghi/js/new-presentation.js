window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {

        const data = await response.json();
        const conferences = data.conferences;
        const ConferenceSelector = document.getElementById('conference');


        for (let conference of conferences) {
            const name = conference.name;
            const id = conference.id;
            const OptionTag = document.createElement('option');
            OptionTag.innerHTML = name;
            OptionTag.value = id;
            ConferenceSelector.appendChild(OptionTag);

        };

        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async event => {

            event.preventDefault();
            const formData = new FormData(formTag);
            const data = Object.fromEntries(formData)
            const json = JSON.stringify(data);
            const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`
            const fetchConfig = {
                method: 'post',
                body: json,
                'Content-type': 'application.json',
            };

            const response = await fetch(presentationUrl, fetchConfig);

            if (response.ok) {
                formTag.reset();
                const data = await response.json();
                console.log(data);
                
            };

        });
    };
});
