import React, { useEffect, useState } from "react";


function ConferenceForm () {

    const [locations, setLocations] = useState([]);
    const [conference, setConference] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: 0,
        max_attendees: 0,
        location: 0
    });

    const handleNameChange = (event) => {
        const value = event.target.value;
        setConference({
            name: value,
            starts: conference.starts,
            ends: conference.ends,
            description: conference.description,
            max_presentations: conference.max_presentations,
            max_attendees: conference.max_attendees,
            location: conference.location
        })
    };

    const handleStartChange = (event) => {
        const value = event.target.value;
        setConference({
            name: conference.name,
            starts: value,
            ends: conference.ends,
            description: conference.description,
            max_presentations: conference.max_presentations,
            max_attendees: conference.max_attendees,
            location: conference.location
        })
    };

    const handleEndChange = (event) => {
        const value = event.target.value;
        setConference({
            name: conference.name,
            starts: conference.starts,
            ends: value,
            description: conference.description,
            max_presentations: conference.max_presentations,
            max_attendees: conference.max_attendees,
            location: conference.location
        })
    };

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setConference({
            name: conference.name,
            starts: conference.starts,
            ends: conference.ends,
            description: value,
            max_presentations: conference.max_presentations,
            max_attendees: conference.max_attendees,
            location: conference.location
        })
    }

    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setConference({
            name: conference.name,
            starts: conference.starts,
            ends: conference.ends,
            description: conference.description,
            max_presentations: +value,
            max_attendees: conference.max_attendees,
            location: conference.location
        })
    }

    const handleAttendeeChange = (event) => {
        const value = event.target.value;
        setConference({
            name: conference.name,
            starts: conference.starts,
            ends: conference.ends,
            description: conference.description,
            max_presentations: conference.max_presentations,
            max_attendees: +value,
            location: conference.location
        })
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setConference({
            name: conference.name,
            starts: conference.starts,
            ends: conference.ends,
            description: conference.description,
            max_presentations: conference.max_presentations,
            max_attendees: conference.max_attendees,
            location: +value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const json = JSON.stringify(conference);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fecthConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-type': 'application.json',
            },
        };
        const response = await fetch(conferenceUrl, fecthConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} name="name" placeholder="Name" required type="text" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartChange} name="starts" placeholder="Starting date" required type="date" id="starts" className="form-control"/>
                            <label htmlFor="room_count">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndChange} name="ends" placeholder="Ending date" required type="date" id="ends" className="form-control"/>
                            <label htmlFor="city">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea onChange={handleDescriptionChange} name="description" required id="description" className="form-control" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePresentationChange} name="max_presentations" placeholder="Max presentations" required type="number" id="max_presentations" className="form-control"/>
                            <label htmlFor="city">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAttendeeChange} name="max_attendees" placeholder="Max attendees" required type="number" id="max_attendees" className="form-control"/>
                            <label htmlFor="city">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} name="location" required id="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map( location => {
                                return (
                                    <option value={location.id} key={location.id}>
                                        {location.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ConferenceForm
