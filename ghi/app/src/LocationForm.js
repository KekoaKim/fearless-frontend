import React, { useEffect, useState } from 'react';

function LocationForm() {
  const [states, setStates] = useState([]);
  // const [name, setName] = useState('');
  // const [room_count, setRoomCount] = useState('');
  // const [city, setCity] = useState('');
  // const [location, setLocation] = useState('');

  const [location, setLocation] = useState({
      name: '',
      room_count: 0,
      city: '',
      state: ''
  })
  const handleRoomCountChange = (event) => {
      const value = event.target.value;
      setLocation({
          name: location.name,
          room_count: +value,
          city: location.city,
          state: location.state
      })
  };

  const handleNameChange = (event) => {
      const value = event.target.value;
      setLocation({
          name: value,
          room_count: location.room_count,
          city: location.city,
          state: location.state
      });
  };

  const handleCityChange = (event) => {
      const value = event.target.value;
      setLocation({
          name: location.name,
          room_count: location.room_count,
          city: value,
          state: location.state
      });
  }

  const handleStateChange = (event) => {
      const value = event.target.value;
      setLocation({
          name: location.name,
          room_count: location.room_count,
          city: location.city,
          state: value
      });
  }

  const handleSubmit = async (event) => {
      event.preventDefault();
      const json = JSON.stringify(location);
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
          method: "post",
          body: json,
          headers: {
              'Content-type': 'application.json',
          },
      }
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
          const newLocation = await response.json();
      }
  }


  const fetchData = async () => {
      const url = 'http://localhost:8000/api/states/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setStates(data.states);

      }
    }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select onChange={handleStateChange} required name="location" id="location" className="form-select">
                <option  value="">Choose a location</option>
                {states.map( state => {
                    return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                    );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;
