import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import MainPage from './MainPage';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined ) {
    return null
  };
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path='/'>
          <Route index element={<MainPage />} />
          <Route path='locations'>
            <Route path='new' element={<LocationForm />} />
          </Route>
          <Route path='attendees'>
            <Route index element={<AttendeesList attendees={props.attendees} />} />
            <Route path='new' element={<AttendeeForm />} />
          </Route>
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />} />
          </Route>
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>

  );
}

export default App;
