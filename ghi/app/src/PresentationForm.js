import React, { useEffect, useState } from "react";


function PresentationForm() {

    const [conferences, setConferences] = useState({
        conferences: [],
        conference: ''
    });
    const [presentation, setPresentation] = useState({
        presenter_name: '',
        company_name: '',
        presenter_email: '',
        title: '',
        synopsis: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences({
                ...conferences,
                conferences: data.conferences
            });
        };

    };

    useEffect(() => {
      fetchData();
    }, []);

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConferences({
            ...conferences,
            conference: value
        })
    };

    const handleInputChange = (event) => {
        const value = event.target.value;
        setPresentation({
            ...presentation,
            [event.target.name] : value
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const json = JSON.stringify(presentation);
        const presentationUrl = `http://localhost:8000/api/conferences/${conferences.conference}/presentations/`;
        const fecthConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-type': 'application.json',
            },
        };

        const response = await fetch(presentationUrl, fecthConfig);

        if (response.ok) {
          event.target.reset();
        };

    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} name="presenter_name" placeholder="Presenter name" required type="text" id="presenter_name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} name="presenter_email" placeholder="Presenter email" required type="email" id="presenter_email" className="form-control"/>
                <label htmlFor="room_count">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} name="company_name" placeholder="Company name" required type="text" id="company_name" className="form-control"/>
                <label htmlFor="city">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} name="title" placeholder="Title" required type="text" id="title" className="form-control"/>
                <label htmlFor="city">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Synopsis</label>
                <textarea onChange={handleInputChange} name="synopsis" required id="synopsis" className="form-control" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} name="conference" required id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.conferences.map(conference => {
                    return (
                        <option value={conference.id} key={conference.id}>
                            {conference.name}
                        </option>
                    )
                  } )}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
};

export default PresentationForm;
